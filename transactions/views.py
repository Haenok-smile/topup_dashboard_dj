from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import User
from app.models import Member
from .forms import DepositForm, WithdrawalForm


@login_required()
def deposit_view(request):
    message = None
    form = DepositForm(request.POST or None)
    title = "Deposit"

    if form.is_valid():
        deposit = form.save(commit=False)
        deposit.user = request.user

        user_id = User.objects.get(username=deposit.user).pk
        member = Member.objects.get(user_id=user_id)
        member.balance += deposit.amount
        member.save()
        deposit.save()

        message = "Success! Your deposit has been done successfuly."

    context = {
        "title": title,
        "form": form,
        "message": message
    }
    
    return render(request, 'transactions/form.html', context)


@login_required()
def withdrawal_view(request):
    message = None
    is_available = True
    form = WithdrawalForm(request.POST or None)
    title = "Withdraw"

    if form.is_valid():
        withdrawal = form.save(commit=False)
        withdrawal.user = request.user

        # checks if user is tring Withdraw more than his balance.
        if withdrawal.user.balance >= withdrawal.amount:
            user_id = User.objects.get(username=deposit.user).pk
            member = Member.objects.get(user_id=user_id)
            member.balance -= deposit.amount
            member.save()
            withdrawal.save()

            message = "Success! Your withdraw has been done successfuly."

        else:
            message = "You Can Not Withdraw More Than You Balance."
            is_available = False

    context = {
        "title": title,
        "form": form,
        "message": message
    }

    return render(request, "transactions/form.html", context)

    
