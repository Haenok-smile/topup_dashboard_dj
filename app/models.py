from django.db import models
from django.contrib.auth.models import User

class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=20)
    balance = models.DecimalField(
        default=0,
        max_digits=12,
        decimal_places=2
    )