from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from . models import Member
from .forms import UserForm, MemberForm
from django.contrib.auth import authenticate, login as auth_login, logout
from django.contrib.auth.decorators import login_required


@login_required
def index(request):
    user = request.user
    balance = Member.objects.filter(user=user).values_list('balance', flat=True)[0]
    return render(request, 'app/index.html', {'user': user, 'balance': balance})


def register(request):
    if request.method == 'POST':
        user_email = request.POST.get('email')
        user_name = request.POST.get('username')
        user_password = request.POST.get('password')
        user_phone = request.POST.get('phone')

        user_form = UserForm(data=request.POST)
        member_form = MemberForm(data=request.POST)

        if user_form.is_valid() and member_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.email = user_form.cleaned_data['email']
            new_user.username = user_form.cleaned_data['username']
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()

            new_member = member_form.save(commit=False)
            new_member.user = new_user
            new_member.save()
            return redirect('login')
    else:
        user_form = UserForm()
        member_form = MemberForm()
    return render(request, 'app/register.html', {'user_form': user_form, 'member_form': member_form})


def login(request):
    error_message = None
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user is not None:
            auth_login(request, user)
            return redirect('index')
        else:
            error_message = 'No login credentials found'
    return render(request, 'app/login.html', {'error_message': error_message})


@login_required
def signout(request):
    logout(request)
    return redirect('login')
