from django.contrib import admin
from django.urls import path, include
from app import views as app_views
from transactions import views as transaction_views

urlpatterns = [
    # jet dashboard
    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    # admin
    path('admin/', admin.site.urls),
    # app 
    path('', app_views.index, name='index'),
    path('register/', app_views.register, name='register'),
    path('login/', app_views.login, name='login'),
    path('logout/', app_views.signout, name='logout'),

    # transactions
    path('deposit/', transaction_views.deposit_view, name='deposit'),
    path('withdraw/', transaction_views.withdrawal_view, name='withdraw'),
]

